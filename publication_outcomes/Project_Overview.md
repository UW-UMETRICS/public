# Inefficient Concentration of Research Funding in the Academic Production of Publications & Patents

The overarching subject of this work is to study the production function for academic research. More specifically we are interested in how various characteristics of the academic research process (eg team composition or Pi investment) affect outcomes (eg. publications or student placement). This work is motivated by the observation that research awards tend to be highly concentrated on a small number of PIs with exceptional publication records. Given that the knowledge production function likely has decreasing marginal returns to team size (and even decreasing returns to number of concurrent projects) and the existence of information asymmetry between funders and researchers, we suspect that there are welfare gains to more diverse distribution of awards.

### Inputs of the Research Process

Previous research on knowledge production has been limited to focusing on monetary inputs and how they affect research outcomes. With UMETRICS we have a much finer picture of the research process and thus are able to investigate more nuanced production inputs. Specifically we will include the following variables in our investigation:

* Award Total
* Team Size
* Team Composition
* Expenditure Profiles (eg. labor vs capital intensive)
* PI Characteristics
 + Quality measures (eg. H-Index)
 + Concentration measures (eg. number of concurrent projects)
* Research Field
* Funding Source Characteristics
 + Public vs Private
 + Remote vs Local

### Outputs of the Research Process

As a first step towards investigating outcomes, in this project we will focus on outcomes that have been commonly studied, specifically

* Publications
* Citations
* Patents

Though these outcomes have received the most attention in science policy research, they only represent a small slice of the potential benefit of academic research. Future work will broaden the list of outcomes and may include

* Direct Expenditures into the Economy.
* Labor Outcomes of Participants.

### Work Plan
The first phase of this project is primarily concerned with data aggregation and linkage. Specifically the following data products must be linked to provide various variables of interest. The importance of the linkages are reflected in their ranking.

1. UMETRICS awards <-> SCOPUS publications (assigned to Anton)
2. USDA PIs <-> Google Scholar author characteristics (assigned to Elan)
3. UMETRICS Fundin Source <-> Info Group firm characteristics
4. UMETRICS Awards <-> Patents