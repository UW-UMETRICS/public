import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), 'scripts'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'scripts', 'source_doc_creation'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'scripts', 'data_processing'))
####################### Retrieving the Processed Data ######################





######################## Validating the Datapackages #######################
print("\nInserting schemas into primary datapackage json file....\n")
import add_schemas_to_datapackage




###################### Creating Sphinx Source Documents ######################
# print("\nCreating sphinx source document for NSF data....\n")
# import make_nsf_doc
print("\nCreating sphinx source document for NPO data....\n")
import make_npo_doc
print("\nCreating sphinx source document for USDA data....\n")
import make_usda_doc
# print("\nCreating sphinx source document for NIH data....\n")
# import make_nih_doc
print("\nCreating sphinx source documents for UMETRICS data tables....\n")
import make_umetrics_header_doc
import make_umetrics_award_doc
import make_umetrics_vendor_doc
import make_umetrics_employee_doc
import make_umetrics_objcodes_doc
import make_umetrics_orgunits_doc
import make_umetrics_subaward_doc

############################### Running Sphinx ###############################
print("\nRunning sphinx for documentation creation....\n")
os.chdir('docs')
# os.system("make html")    # Old way (inelegant)
fn = __file__
lst = [fn, '-b', 'html', '-d', 'build/doctrees', 'source', 'build/html']
from sphinx import main
main(lst)
