# This script reads in the main datapackage json file and inserts any schemas
#	that are referenced by address (instead of written direcctly into the doc
#	itself)

import os

# Checking if current working directory is in root or scripts (and adjusting addresses)
current_folder = os.getcwd().split('\\')[-1]
if current_folder == 'proj_root':
	prefix = ''
elif current_folder == 'scripts':
	prefix = '../'
else:
	print('The current working folder is unrecognized.')

filein = open(prefix + "datapackage.json", "r")
fileout = open(prefix + "datapackage_with_schema.json", "w")

for line in filein:
	# Checking for schema lines that contain urls (identified by the existence of json)
	if (line.find("schema") >= 0  and line.find("json") >= 0):
		fileout.write("            \"schema\": \n")
		indentSpace = line[0:line.find("\"")-1] + "    "
		# Extract address of schema
		afterTag = line[line.find("schema")+8:]
		firstQuote = afterTag.find("\"")
		url = afterTag[firstQuote+1:afterTag.find("\"",firstQuote+2)]
		print("Inserting schema from file: " + url)
		if url[0] == ".":
			url = url[2:]

		# Writing schema file (with indentation) to datapackage file
		schemafile = open(prefix + url, "r")
		for schemaline in schemafile:
			fileout.write(indentSpace+schemaline)
		#fileout.write(schemafile.read())
		schemafile.close()
	else:
		fileout.write(line)

filein.close()
fileout.close()

print("Saving file with inserted schemas as datapackage_with_schema.json")
