import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

# Note: paths should be relative to proj_root directory
datapackagefile = "datapackage_with_schema.json"
datafile = "data/nsf-awards/nsf_proc(He).csv"  # This needs to match the path in the datapackage
outputdoc = "docs/source/nsf_doc.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
data = pd.read_csv(datafile)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("National Science Foundation (NSF) Awards Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nEntity: the National Science Foundation (NSF) \n\nLink: ")
fileout.write(dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])


""" --------------------------------------Descriptive Section ------------------------------------"""
fileout.write(rst.print_subtitle("Descriptive Statistics"))

#Determine the number of rows and columns in the file
fileout.write("\n| Number of columns in the file: %s" % data.shape[1])
fileout.write("\n| Number of rows in the file: %s" % data.shape[0])

# Determine the mean of the "award_amount" column
award_amount_mean = int(data["award_amount"].mean())
fileout.write("\n| Average Amount Awarded: $%s" % award_amount_mean)

# Determine the max value in the "award_amount" column
award_amount_max = data["award_amount"].max()
fileout.write("\n| Maximum Amount Awarded: $%s " % award_amount_max)

# Zero, the number (0), is the min value. Determine how many times that value appears in the data
award_amount_zero_count = 0
for value in data["award_amount"]:
	if value == 0:
		award_amount_zero_count += 1
fileout.write("\n| Number of Awards that gave $0: %s" % award_amount_zero_count)
#fileout.write("<br>")

# Determine the 10 Universities who gave the most awards and print in table
university_occurences = data["institution"].value_counts()[0:10]
fileout.write("\n\n.. table:: Institutions who Gave the Most Awards\n\n")
tableout = tabulate(university_occurences.to_frame(), headers=['Institution','Awards'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))


""" ---------------------------------------- Quirks Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Quirks"))
fileout.write("\n As you can see from the \"Descriptive\" section, there are some names (David and Michael) that appear as institutions in the data. This is happening often for a variety of names, with some very common names showing up frequently. The reason for this error is because the program used to parse the XML data (which is the format in which the NSF provides data) doesn't account for multiple principle investigators (PI) on one project. As a result, some PI first names are ending up in the institution field. When the new XML parser is created, it should either A) take into account the possibility of multiple PIs or B) Only list the first PI and then ensure that the institution field is filled with the data in the institution tag. NOTE that the datapackage is currently set up such that option B is used (only one PI first name and PI last name field), and that if option A is chosen then the datapackage should be edited to include multiple PIs.")

fileout.close()
