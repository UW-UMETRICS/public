import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

# Note: paths should be relative to proj_root directory
datapackagefile = "datapackage_with_schema.json"
#datafile = "data/nsf-awards/nsf_proc(He).csv"  # This needs to match the path in the datapackage
outputdoc = "docs/source/umetrics_header.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
#data = pd.read_csv(datafile)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")


""" ----------------------------------- Overview Section -------------------------------------"""

fileout.write(rst.print_title("UMETRICS - UW Madison Data"))
fileout.write("Here will be an introduction to the UMETRICS data.\n\n")

""" --------------------------------------- Tables -----------------------------------------"""

fileout.write(rst.print_subtitle("Tables Included"))
fileout.write("There are several associated tables in this data package:\n\n")
fileout.write(".. toctree:: \n   :maxdepth: 1\n\n")
fileout.write("   Awards Data <umetrics_award>\n")
fileout.write("   Employee Data <umetrics_employee>\n")
fileout.write("   Vendor Data <umetrics_vendor>\n")
fileout.write("   Subaward Data <umetrics_subaward>\n")
fileout.write("   Organizational Units <umetrics_orgunits>\n")
fileout.write("   Object Codes <umetrics_objcodes>\n")

fileout.write("\n\nFor brief descriptions of each of the tables listed above refer below:\n\n")



# """ ---------------------------------------- Quirks Section ---------------------------------------"""
# fileout.write(rst.print_subtitle("Quirks"))
# fileout.write("\n As you BLHAAHAHAHAHAHAHAHAH.")

fileout.close()
