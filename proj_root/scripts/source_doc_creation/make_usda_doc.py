import pandas as pd
import datapackage
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate
import numpy as np
import rst_maker as rst

# Note: paths should be relative to compile_all.py because that is where these are being executed
datapackagefile = "datapackage_with_schema.json"
datafile = "data/usda-awards/USDA_awards_proc.csv"
outputdoc = "docs/source/usda_doc.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
df = pd.read_csv(datafile, encoding='ISO-8859-1', low_memory=False)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("US Department of Agriculture (USDA) Awards Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nRaw Data Source: " + dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])

""" --------------------------------------Descriptive Section ------------------------------------"""
fileout.write("\n\n")
fileout.write(rst.print_subtitle("Descriptive Statistics"))

count = df["recipient_organization_type"].value_counts()
fileout.write("\n\n.. table:: Top Recipient Organization Types\n\n")
tableout = tabulate(count.to_frame(), headers=['Recipient Type','Count'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")

count = df["recipient_organization"].value_counts()[0:10]
fileout.write("\n\n.. table:: Top Recipient Organizations\n\n")
tableout = tabulate(count.to_frame(), headers=['Recipient Org.','Count'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")

# countsum = df.groupby(df.recipient_organization).agg({'accession_number':'count', 'total_all_funds':'sum'})
# countsum.columns = ['Count', 'Sum_Funds']
# countsum = countsum.sort_values('Count', ascending = False)
# countsum = countsum[0:10]

# print("**********")
# count = df["recipient_organization_type"].value_counts()
# print("**********")
# print("Recipient Organization Type Count\n")
# print(count);
# print("**********\n")

## TODO: Fix calculation so it looks at proper fields
awards = df[df["total_all_funds"] >= 0]
stats = awards["total_all_funds"].describe()
fileout.write("\n\n.. table:: Summary Statistics for 'Total All Funds'\n\n")
tableout = tabulate(stats.to_frame(), headers=['Statistic','Value'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")

stats = df["cumulative_award_amount"].describe()
fileout.write("\n\n.. table:: Summary Statistics for 'Cumulative Award Amount'\n\n")
tableout = tabulate(stats.to_frame(), headers=['Statistic','Value'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")

## TODO: This field is not in the larger usda data set.... must investigate
# field = df["Field of Science"].value_counts()
# print("**********")
# print("Top 20 Field of Science for Awards")
# print(field[0:20])
# print("**********\n")

fund = df["funding_source"].value_counts()
fileout.write("\n\n.. table:: Top Funding Source Types\n\n")
tableout = tabulate(fund.to_frame(), headers=['Funding Source','Count'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  ").replace("Â", " "))

# fund = df["funding_source"].value_counts()
# print("**********")
# print("Funding Source")
# print(fund)
# print("**********\n")

""" ---------------------------------------- Quirks Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Quirks"))

fileout.write("The data contains a lot of numerical values under 0")
