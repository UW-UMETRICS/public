import pandas as pd
import datapackage
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate
import numpy as np
import rst_maker as rst
import locale # For formatting currency

locale.setlocale( locale.LC_ALL, '')

# Note: paths should be relative to compile_all.py because that is where these are being executed
datapackagefile = "datapackage_with_schema.json"
datafile = "data/nih-awards/RePORTER_PRJ_C_All_Years.csv"
outputdoc = "docs/source/nih_doc.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
df = pd.read_csv(datafile, encoding='ISO-8859-1')
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("National Institute of Health (NIH) Awards Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nRaw Data Source: " + dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])

""" --------------------------------------Descriptive Section ------------------------------------"""
fileout.write("\n\n")
fileout.write(rst.print_subtitle("Descriptive Statistics"))

# Determine the number of rows and columns in the file
fileout.write("\n| Number of columns in the file: %s" % df.shape[1])
fileout.write("\n| Number of rows in the file: %s" % df.shape[0])

fileout.write("\n\n|")

# TODO: Currently this just looks at the field, but the field can contain multiple PIs delimited by ; so ideally we would first break those up and then aggregate
top_PIs = df["PI_NAMEs"].value_counts()[0:10]
fileout.write("\n\n.. table:: Top 10 Principal Investigators (by award count)\n\n")
tableout = tabulate(top_PIs.to_frame(), headers=['Principal Investigators','Awards'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")

count = df.groupby('PI_NAMEs')['TOTAL_COST'].sum().sort_values(ascending = False)[0:10]
# Converting numbers to currency
top_PIs = count.map((lambda x: locale.currency(x, grouping = True)))
fileout.write("\n\n.. table:: Top 10 Principal Investigators (by award value)\n\n")
tableout = tabulate(top_PIs.to_frame(), headers=['Principal Investigators','Total Award Value'], tablefmt="grid")
fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.write("\n\n|")
