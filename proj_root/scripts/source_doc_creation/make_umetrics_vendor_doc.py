import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate
import locale # For formatting currency

locale.setlocale( locale.LC_ALL, '')

# Note: paths should be relative to proj_root directory
datapackagefile = "datapackage_with_schema.json"
datafile = "data/universities/uw-madison/UMETRICS_All_Years_Vendor.csv"  # This needs to match the path in the datapackage
outputdoc = "docs/source/umetrics_vendor.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
data = pd.read_csv(datafile, low_memory=False)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("UMETRICS - Vendor Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

# TODO: Try to have description of ObjCode and OrgUnit fields have links to those documentation pages

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nData Source: ")
fileout.write(dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])


# """ --------------------------------------Descriptive Section ------------------------------------"""
# fileout.write(rst.print_subtitle("Descriptive Statistics"))

# # Determine the number of rows and columns in the file
# fileout.write("\n| Number of columns in the file: %s" % data.shape[1])
# fileout.write("\n| Number of rows in the file: %s" % data.shape[0])

# # This tabulates the top vendors by number of transactions
# top_vendors = data["OrgName"].value_counts()[0:10]
# fileout.write("\n\n.. table:: Top 10 Vendors (by transactions) \n\n")
# tableout = tabulate(top_vendors.to_frame(),
# 					headers=['Vendor','Transactions'],
# 					tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))

# fileout.write("\n\n|")

# # This tabulates the top vendors by value of total transactions
# data['AllPayments'] = data.groupby('OrgName')["VendorPaymentAmount"].transform(sum)
# top_vendors  = data.sort_values('AllPayments', ascending = False)
# top_vendors  = top_vendors.drop_duplicates(['OrgName'])[['OrgName', 'AllPayments']][0:10]
# # Converting numbers to currency
# top_vendors.AllPayments = top_vendors.AllPayments.map((lambda x: locale.currency(x, grouping = True)))
# fileout.write("\n\n.. table:: Top 10 Vendors (by transactions value) \n\n")
# tableout = tabulate(top_vendors, showindex = False,
# 					headers=['Vendor','Transactions Total Value'],
# 					tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))

# fileout.write("\n\n|")

# # Calculating Top Transaction by Type (Object Code)
# obj_codes = pd.read_csv("data/universities/uw-madison/UMETRICS_All_Years_ObjectCodes.csv", low_memory=False)
# data = data.merge(obj_codes, how = 'left', on = ['ObjectCode', 'data_file_year'])
# top_transactions = data["ObjectCodeText"].value_counts()[0:10]
# fileout.write("\n\n.. table:: Top 10 Transaction Types \n\n")
# tableout = tabulate(top_transactions.to_frame(),
# 					headers=['Transaction Type','Transactions'],
# 					tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.close()
