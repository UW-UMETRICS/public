import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

# Note: paths should be relative to proj_root directory
datapackagefile = "datapackage_with_schema.json"
datafile = "data/universities/uw-madison/UMETRICS_All_Years_Award.csv"  # This needs to match the path in the datapackage
outputdoc = "docs/source/umetrics_award.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
data = pd.read_csv(datafile)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("UMETRICS - Awards Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nData Source: ")
fileout.write(dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])


# """ --------------------------------------Descriptive Section ------------------------------------"""
# fileout.write(rst.print_subtitle("Descriptive Statistics"))

# # Determine the number of rows and columns in the file
# fileout.write("\n| Number of columns in the file: %s" % data.shape[1])
# fileout.write("\n| Number of rows in the file: %s" % data.shape[0])

# fileout.write("\n\n| Average Total Expenditure: $%s " % str(round(data["TotalExpenditures"].mean(), 2)))
# fileout.write("\n| Total Expenditures: $%s " %  str(round(data["TotalExpenditures"].sum(), 2)))

# #data.columns
# award_occurences = data["FundingSource"].value_counts()[0:10]
# fileout.write("\n\n.. table:: Top 10 Funding Sources (by transactions)\n\n")
# tableout = tabulate(award_occurences.to_frame(), headers=['Funding Source','Transactions'], tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.close()
