import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

# Note: paths should be relative to compile_all.py because that is where these are being executed
datapackagefile = "datapackage_with_schema.json"
datafile = "data/npos-irs/IRS.csv"
outputdoc = "docs/source/npo_doc.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
irs = pd.read_csv(datafile)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("Nonprofit IRS Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nRaw Data Source: " + dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])

""" --------------------------------------Descriptive Section ------------------------------------"""
fileout.write(rst.print_subtitle("Descriptive Statistics"))

# Calculating the mean cash grant and displaying it
cash_mean = irs["amount_of_cash_grant"].mean()
fileout.write("Average Cash Grant: $%s\n\n" % round(cash_mean, 2))

# Calculating the mean total number of 501(c)(3) and govt. grants and displaying it
gov_grants = irs["total_nbr_of501_c3_and_govt_grants"].mean()
fileout.write("Average # of 501(c)(3) and govt. grants: %s\n\n" % round(gov_grants, 2))

# Calculating the mean total number of other organizations and displaying it
other_orgs = irs["total_nbr_of_other_organizations"].mean()
fileout.write("Average # of other organizations: %s\n" % round(other_orgs,2))

fileout.write('\n|\n')

# Null finder: finding # of null items for each field
fields = list(irs.columns.values)
nullsDF = pd.DataFrame(columns = ['Field', 'Nulls'])
nullsDF.Nulls = nullsDF.Nulls.astype('int64')
i = 0
for field in fields:
    field_data = irs[field]
    null_list = pd.isnull(field_data)
    not_null = field_data[null_list == False]
    true_count = len(not_null)
    count = len(field_data)
    nullsDF.loc[i] = [field, (int)(count - true_count)]
    i = i + 1
    #fileout.write("Number of null entries in %s: %s\n\n" % (field, (int)(count - true_count)))
fileout.write("\n.. table:: Number of Missing Entries By Field\n\n")
tableout = tabulate(nullsDF, headers='keys', tablefmt="grid", showindex=False)
fileout.write("  " + tableout.replace("\n", "\n  "))
#fileout.write("Total # of entries: %s\n\n" % len(irs[fields[0]]))

fileout.write('\n\n|\n')

# Counter: finding the # of entries per year
fileout.write("\nData ranged from 2008 through 2015. Total records by year:\n")
unique_vals = (irs['tax_year'].unique())

total_in_cat = dict()
value_column = irs['tax_year']

for val in unique_vals:
    counter = 0
    for row in value_column:
        if row == val:
            counter += 1
    total_in_cat[val] = counter

fileout.write("\n")

for key, value in total_in_cat.items():
    fileout.write(":%s: %s\n" % (key, value))

fileout.close()
