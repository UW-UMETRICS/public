README Skeleton

## Description


8 Tables
Award: Expenditures by award
Employee: Percentage of earnings spent on award, grouped by employee
EmployeeWName: Link employeee names to Employee file
Vendor: Vendor payments grouped by vendor
Sub Award: Sub-Award payments grouped by sub award recipient. 
Organizational Unit: Ancillary table for matching sub-organizations with their parent organization including a key (suborgunit)  
Object Codes: Ancillary table to describe the object coding in the employee, vendor and subaward transactional tables

## Source
Entity: Institute for Research on Innovation & Science
Website: http://iris.isr.umich.edu/

## File Sizes (1 year)

Award: 10 Fields || 56792 Rows
Employee: 11 Fields || 151012 Rows
EmployeeWNames: 12 Fields || 151012 Rows
Vendor: 20 Fields || 25183 Rows
Subaward: 20 Fields || 4365 Rows
Organizational Unit: 4 Fields || 28 Rows 
Object Codes: 1 Field || 536 Rows

## Descriptive

**Award CSV:**
Average Amount Awarded: $11474.51
Total Amount Awarded: $651660154.76

Ten institutions who gave the most awards: 
WISCONSIN ALUMNI RESEARCH FOUNDATION        11823
DHHS, PHS, NATIONAL INSTITUTES OF HEALTH     8490
NATIONAL SCIENCE FOUNDATION                  5381
HATCH ACT FORMULA FUND                       1152
WISCONSIN DEPT OF HEALTH SERVICES             903
DOE, CHICAGO OPERATIONS OFFICE                798
NASA, GODDARD SPACE FLIGHT CENTER             707
USDA, AGRICULTURAL RESEARCH SERVICE           653
WISCONSIN DEPT OF TRANSPORTATION              647
USDA, NATL INSTITUTE FOOD & AGRICULTURE       635


**Employee CSV**

Employee Total Rows: 151012
Who received the awards? (Occupational Classification)
Graduate Student                40277
Faculty                         36689
Technician/Staff Scientist      28455
Undergraduate Students          19551
Research Support                12147
Post Graduate Research           9495
Clinician                        2235
Research Analyst/Coordinator     2148
Research support                   15
Name: occupationalclassification, dtype: int64

Who received the awards? (Job Title)
RESEARCH ASSISTANT     25676
STUDENT HELP           19535
PROFESSOR              12511
RESEARCH ASSOCIATE      7495
FELLOW                  5346
PROJECT ASST-REG        4976
ASSISTANT SCIENTIST     4777
ASSOCIATE PROFESSOR     3877
RESEARCH SPECIALIST     3790
ASSOC RESEARCH SPEC     3618


**Subaward CSV**

Ten Organizations That Gave the Most Subawards:
Regents of the University of Minnesota    151
JOHNS HOPKINS UNIVERSITY                   95
STANFORD UNIVERSITY                        91
WASHINGTON UNIVERSITY                      88
PENNSYLVANIA STATE UNIVERSITY              82
MARSHFIELD CLINIC RESEARCH FND             80
MICHIGAN STATE UNIVERSITY                  77
IOWA STATE UNIVERSITY                      71
NORTHWESTERN UNIVERSITY                    68
UNIVERSITY OF TEXAS SOUTHWESTERN           65


## Quirks


This is for only one year of data, but everything appears to be in good order. There does appear to be some missing data in the tables that I will investigate further, but mostly in non-essential fields. 
