# This module contains variables and methods that are used repeatedly throughout
#   the scripts that generate the rst files to input into sphinx.
import os, pandas as pd
import datapackage
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

titleBar = "==========================================================================="
subtitleBar = "---------------------------------------------------------------------------"
subsubtitleBar = "#############################################################################"

def load_helper_vars():
    # Defines variables to delineate sections in an .rst file
    global titleBar
    global subtitlebar
    global subsubtitlebar
    titleBar = "==========================================================================="
    subtitleBar = "---------------------------------------------------------------------------"
    subsubtitleBar = "#############################################################################"

def navigate_to_root():
    # This will navigate the current working directory to the proj_root folder
    current_folder = os.getcwd().split('\\')[-1]
    count = 0
    while ( current_folder != 'proj_root' ):
        #print(os.getcwd())
        os.chdir('..')
        current_folder = os.getcwd().split('\\')[-1]
        count = count + 1
        if count > 5:   break
    if current_folder != 'proj_root':
        print('Directory proj_root not found. Must not have been run from within sphinx structure.')
    else:
        #print('Changed directory to: '+ os.getcwd())
        return

def print_title(title):
    return '\n' + titleBar + '\n' + title + '\n' + titleBar + '\n'

def print_subtitle(title):
    return '\n\n' + title + '\n' + subtitleBar + '\n'

def print_subsubtitle(title):
    return '\n\n' + title + '\n' + subsubtitleBar + '\n'

def print_field_desc_meta(dp, resourceIndex):
    # Returns a table of the field and description metadata in the datapackage
    #   file (dp) for the resource at resourceIndex

    # Extracting descriptions of all the fields
    fieldmeta = pd.DataFrame([{'Field':field['name'], 'Description':field['description']}
                 for field in dp.resources[resourceIndex].descriptor['schema']['fields']])
    fieldmeta = fieldmeta[['Field', 'Description']]

    return tabulate(fieldmeta, headers="keys", tablefmt="grid", showindex=False, stralign="right")
