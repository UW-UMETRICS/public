import pandas as pd
import datapackage
import rst_maker as rst
from tabulate import tabulate  # Documentation here: https://pypi.python.org/pypi/tabulate

# Note: paths should be relative to proj_root directory
datapackagefile = "datapackage_with_schema.json"
datafile = "data/universities/uw-madison/UMETRICS_All_Years_Subaward.csv"  # This needs to match the path in the datapackage
outputdoc = "docs/source/umetrics_subaward.rst"

# Changing directory to proj_root
rst.navigate_to_root()

# Reading in data, datapackage, and opening output file
data = pd.read_csv(datafile)
dp = datapackage.DataPackage(datapackagefile)
fileout = open(outputdoc,"w")

# Grabbing index of resource corresponding table in the datapackage
paths = [table.descriptor['path'] for table in dp.resources]
resInd = paths.index('./'+datafile)

""" ----------------------------------- Description Section -------------------------------------"""

fileout.write(rst.print_title("UMETRICS - Subaward Data"))
fileout.write(dp.resources[resInd].descriptor['description'])

""" ----------------------------------------Fields Section ---------------------------------------"""

fileout.write(rst.print_subtitle("Field Information"))
# Printing table of field names and descriptions
fileout.write(rst.print_field_desc_meta(dp, resInd))

# TODO: Try to have description of ObjCode and OrgUnit fields have links to those documentation pages

""" ----------------------------------------Source Section ---------------------------------------"""
fileout.write(rst.print_subtitle("Source and Preprocessing"))
fileout.write("\nData Source: ")
fileout.write(dp.resources[resInd].descriptor['url'])
fileout.write("\n\n" + dp.resources[resInd].descriptor['processing'])


# """ --------------------------------------Descriptive Section ------------------------------------"""
# fileout.write(rst.print_subtitle("Descriptive Statistics"))

# # Determine the number of rows and columns in the file
# fileout.write("\n| Number of columns in the file: %s" % data.shape[1])
# fileout.write("\n| Number of rows in the file: %s" % data.shape[0])

# #data.columns
# organization_occurences = data["OrgName"].value_counts()[0:10]
# fileout.write("\n\n.. table:: Top 10 Subaward Recipients \n\n")
# tableout = tabulate(organization_occurences.to_frame(),
# 					headers=['Subaward Recipient','Subawards'],
# 					tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))
# # TODO: Add column with value of all the Subawards

# fileout.write("\n\n|")

# state_occurences = data["State"].value_counts()[0:10]
# fileout.write("\n\n.. table:: Top 10 States Recieving Subawards \n\n")
# tableout = tabulate(state_occurences.to_frame(),
# 					headers=['State','Subawards'],
# 					tablefmt="grid")
# fileout.write("  " + tableout.replace("\n", "\n  "))

fileout.close()
