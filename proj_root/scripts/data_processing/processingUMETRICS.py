import pandas as pd

raw_dir = '././data/universities/uw-madison/raw_data/'
proc_dir = '././data/universities/uw-madison/processed_data/'

#### Specification of Which Processed Tables To Create #####
makeYearAgg = False
makeTransactionAgg = True
addEmpListToTransAgg = False

############################################################
################   Aggregating Across Years ################
############################################################
if (makeYearAgg):
	# Creating list of years to iterate over
	years = list(range(2008,2017))   # Note: list does not include 2017
	years = [str(i) for i in years]  # Converting to strings

	# List of UMETRICS table types
	UMET_Tables = ['_Award', '_Employee', '_EmployeeWNames', '_ObjectCodes', '_OrganizationalUnit', '_Subaward', '_Vendor']

	# Iterating through different sets of UMETRICS table types
	for tab_suffix in UMET_Tables:

		listOfDFs = []
		# Iterating though files and creating a list of dataframes of the table type
		for y in years:
		    fileName = 'UMETRICS_'+y+tab_suffix
		    extension = '.csv'
		    print('Processing file: '+fileName+extension)
		    # Reading the file and appending to list of dataframes
		    singleDataYearFile = pd.read_csv(raw_dir+fileName+extension)
		    singleDataYearFile['data_file_year'] = y
		    listOfDFs.append(singleDataYearFile)

		result = pd.concat(listOfDFs)

		# Some potential code for adjusting variable names (does nothing if old name is not in df)
		newColNameDict = {'old_col_1_name': 'new_col_1_name', 
							'old_col_2_name': 'new_col_2_name',
							'old_col_3_name': 'new_col_3_name'}
		result.rename(columns = newColNameDict, inplace=True)

		print('Saving output file: '+'UMETRICS_All_Years'+tab_suffix+'.csv')
		result.to_csv(proc_dir+'UMETRICS_All_Years'+tab_suffix+'.csv', index=False)

#########################################################
#######   Aggregating Transaction Within Projects #######
#########################################################
if (makeTransactionAgg):
	# Creating Award file with aggregated transactions (ie at the project level instead of transaction level)
	umet = pd.read_csv(proc_dir+'UMETRICS_All_Years_Award.csv')
	grouped = umet.groupby(['UniqueAwardNumber'])  #, 'data_file_year'])
	aggFuncs = {'PeriodStartDate': 'first', 
				'PeriodEndDate': 'last',
				'FundingSource': 'first',
				'RecipientAccountNumber': 'first',
				'AwardTitle': 'first',
				'OverheadCharged': 'sum',
				'TotalExpenditures': 'sum',
				'CampusID': 'first',
				'SubOrgUnit': 'mean',
				'data_file_year': ['min', 'max', 'size']}
	umetAgg = grouped.agg(aggFuncs)
	umetAgg.reset_index(level=['UniqueAwardNumber'], inplace=True)
	umetAgg.columns = [' '.join(col).strip() for col in umetAgg.columns.values]   # This collapses hierarchical columns names
	# umetAgg.columns = ['UniqueAwardNumber', 'CampusID', 'AwardTitle', 'FundingSource', 'TotalOverheadCharged', 'RecipientAccountNumber', 
	# 					'TotalExpenditures', 'ProjStartDate', 'SubOrgUnit', 'data_file_year_earliest', 'data_file_year_latest', 
	# 					'NumberOfTransactions',	'ProjEndDate']
	newColNames = {'PeriodStartDate first': 'FirstTransDate', 
				'PeriodEndDate last': 'LastTransDateDate',
				'FundingSource first': 'FundingSource',
				'RecipientAccountNumber first': 'RecipientAccountNumber',
				'AwardTitle first': 'AwardTitle',
				'OverheadCharged sum': 'TotalOverheadCharged',
				'TotalExpenditures sum': 'TotalExpenditures',
				'CampusID first': 'CampusID',
				'SubOrgUnit mean': 'SubOrgUnit',
				'data_file_year min': 'data_file_year_earliest',
				'data_file_year max': 'data_file_year_latest',
				'data_file_year size': 'NumberOfTransactions'}
	umetAgg.rename(columns=newColNames, inplace=True)
	# Changing column order
	umetAgg = umetAgg[['UniqueAwardNumber', 'AwardTitle', 'FundingSource', 'FirstTransDate', 'LastTransDateDate', 
						'RecipientAccountNumber', 'TotalOverheadCharged', 'TotalExpenditures', 'NumberOfTransactions',
						'data_file_year_earliest', 'data_file_year_latest', 'SubOrgUnit', 'CampusID']]
	print('Saving output file: '+'UMETRICS_All_Years_Award_Aggregated.csv')
	umetAgg.to_csv(proc_dir+'UMETRICS_All_Years_Award_Aggregated.csv', index=False)
