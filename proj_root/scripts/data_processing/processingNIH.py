import pandas as pd

raw_dir = '././data/nih-awards/raw_data/'
proc_dir = '././data/nih-awards/processed_data/'

# Creating list of years to iterate over
years = list(range(2008,2017))   # Note: list does not include 2017
years = [str(i) for i in years]  # Converting to strings

listOfDFs = []
# Iterating though files and creating a list of dataframes of the table type
for y in years:
    fileName = 'RePORTER_PRJ_C_FY'+y
    extension = '.csv'
    print('Processing file: '+fileName+extension)
    # Reading the file and appending to list of dataframes
    singleDataYearFile = pd.read_csv(raw_dir+fileName+extension, encoding = "ISO-8859-1", low_memory=False)
    singleDataYearFile['data_file_year'] = y
    listOfDFs.append(singleDataYearFile)
    print(singleDataYearFile.shape)

# Concatenating all the years
result = pd.concat(listOfDFs)

# Some potential code for adjusting variable names (does nothing if old name is not in df)
newColNameDict = {'old_col_1_name': 'new_col_1_name', 
					'old_col_2_name': 'new_col_2_name',
					'old_col_3_name': 'new_col_3_name'}
result.rename(columns = newColNameDict, inplace=True)

print('Saving output file: '+'RePORTER_PRJ_C_All_Years.csv')
result.to_csv(proc_dir+'RePORTER_PRJ_C_All_Years.csv', index=False)

resultSample = result.sample(n=50000)
print('Saving output file: '+'RePORTER_PRJ_C_All_Years_Sample.csv')
resultSample.to_csv(proc_dir+'RePORTER_PRJ_C_All_Years_Sample.csv', index=False)