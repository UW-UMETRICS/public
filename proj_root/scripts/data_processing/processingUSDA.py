import pandas as pd
import sys

raw_dir = '././data/usda-awards/raw_data/'
proc_dir = '././data/usda-awards/processed_data/'

# Opening entire raw data file
try:
	df = pd.read_csv(raw_dir+'usda_awards_all_fields.csv', encoding = "ISO-8859-1", low_memory=False)
except:
	print("Check if directory of file has changed.")
	exit()

df.columns = map(str.lower, df.columns)
df.columns = df.columns.str.replace(',','')
df.columns = df.columns.str.replace('financial: ', '')
df.columns = df.columns.str.replace('line ', '')
newColDict = {'financial data code (fdc)':'financial_data_code',
			  'other nifa (inc ah sec1433 evans-allen)':'other nifa',
			  'recipient congressional district':'recipient cong district',
			  'research developmental percent':'research dev percent',
			  'eligible for nifa capacity funds':'nifa capacity funds eligibility',
			  'recipient city name':'recipient_city',
			  'other federal funds & reimbursable agmts':'other_fed_funds_&_reimbursable_agmts'}
df = df.rename(columns = newColDict)
df.columns = df.columns.str.replace(' ', '_')

# Writing Output file
newfilename = 'USDA_awards_proc.csv'
print('Saving output file: '+newfilename)
df.to_csv(proc_dir+newfilename, index=False)