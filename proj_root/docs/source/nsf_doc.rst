
===========================================================================
National Science Foundation (NSF) Awards Data
===========================================================================
This dataset includes information on the awards given to universities by the National Science Foundation (NSF). Some of the key fields in the dataset include the dollar amount awarded, the institution receiving the award, and the principal investigators of the research project. This dataset is going to be as it is currently filled with many errors (see "Quirks" section) and only contains part of the full data. Data is available from 2008 to 2017. 

Field Information
---------------------------------------------------------------------------
+---------------+--------------------------------------------+
|         Field |                                Description |
+===============+============================================+
|       tableid |                Primary key for this table. |
+---------------+--------------------------------------------+
|   award_title |                        Title of the Award. |
+---------------+--------------------------------------------+
|  award_number |                    Unique Award ID Number. |
+---------------+--------------------------------------------+
|  award_amount |                       Amount of the Award. |
+---------------+--------------------------------------------+
|   institution | Institution of the Principal Investigator. |
+---------------+--------------------------------------------+
| pi_first_name |  First name of the Principal Investigator. |
+---------------+--------------------------------------------+
|  pi_last_name |   Last name of the Principal Investigator. |
+---------------+--------------------------------------------+

Source and Preprocessing
---------------------------------------------------------------------------

Entity: the National Science Foundation (NSF) 

Link: https://www.nsf.gov/awardsearch/download.jsp

Describe any preprocessing here.

Descriptive Statistics
---------------------------------------------------------------------------

| Number of columns in the file: 7
| Number of rows in the file: 123432
| Average Amount Awarded: $467602
| Maximum Amount Awarded: $884135770 
| Number of Awards that gave $0: 170

.. table:: Institutions who Gave the Most Awards

  +--------------------------------------------+----------+
  | Institution                                |   Awards |
  +============================================+==========+
  | University of Michigan Ann Arbor           |     1252 |
  +--------------------------------------------+----------+
  | University of California-Berkeley          |     1084 |
  +--------------------------------------------+----------+
  | David                                      |     1073 |
  +--------------------------------------------+----------+
  | Georgia Tech Research Corporation          |     1066 |
  +--------------------------------------------+----------+
  | University of Illinois at Urbana-Champaign |     1066 |
  +--------------------------------------------+----------+
  | University of Washington                   |     1063 |
  +--------------------------------------------+----------+
  | Michael                                    |     1015 |
  +--------------------------------------------+----------+
  | Massachusetts Institute of Technology      |      988 |
  +--------------------------------------------+----------+
  | University of Texas at Austin              |      980 |
  +--------------------------------------------+----------+
  | University of Wisconsin-Madison            |      918 |
  +--------------------------------------------+----------+

Quirks
---------------------------------------------------------------------------

 As you can see from the "Descriptive" section, there are some names (David and Michael) that appear as institutions in the data. This is happening often for a variety of names, with some very common names showing up frequently. The reason for this error is because the program used to parse the XML data (which is the format in which the NSF provides data) doesn't account for multiple principle investigators (PI) on one project. As a result, some PI first names are ending up in the institution field. When the new XML parser is created, it should either A) take into account the possibility of multiple PIs or B) Only list the first PI and then ensure that the institution field is filled with the data in the institution tag. NOTE that the datapackage is currently set up such that option B is used (only one PI first name and PI last name field), and that if option A is chosen then the datapackage should be edited to include multiple PIs.