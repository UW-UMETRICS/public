
===========================================================================
UMETRICS - Object Codes
===========================================================================
Ancillary table to describe the object coding in the employee, vendor and subaward transactional.

Field Information
---------------------------------------------------------------------------
+----------------+------------------------------------------------------------------------------------------+
|          Field |                                                                              Description |
+================+==========================================================================================+
|     ObjectCode | Internal organization object code or other expense type category assigned to transaction |
+----------------+------------------------------------------------------------------------------------------+
| ObjectCodeText |                                                           Explanation of the Object Code |
+----------------+------------------------------------------------------------------------------------------+
| data_file_year |                        The year of the data file submission that this record resides in. |
+----------------+------------------------------------------------------------------------------------------+

Source and Preprocessing
---------------------------------------------------------------------------

Data Source: Restricted VDI managed by the Institute for Research on Innovation & Science (IRIS)

Describe any preprocessing here.