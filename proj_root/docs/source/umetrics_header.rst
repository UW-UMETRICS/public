
===========================================================================
UMETRICS - UW Madison Data
===========================================================================
Here will be an introduction to the UMETRICS data.



Tables Included
---------------------------------------------------------------------------
There are several associated tables in this data package:

.. toctree:: 
   :maxdepth: 1

   Awards Data <umetrics_award>
   Employee Data <umetrics_employee>
   Vendor Data <umetrics_vendor>
   Subaward Data <umetrics_subaward>
   Organizational Units <umetrics_orgunits>
   Object Codes <umetrics_objcodes>


For brief descriptions of each of the tables listed above refer below:

