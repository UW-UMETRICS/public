.. UMETRICS documentation master file, created by
   sphinx-quickstart on Sat Jun 24 12:03:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UMETRICS's documentation!
====================================

This data product contains both University submitted data on project awards (UMETRICS) and various agency tables that supplement the information in the UMETRICS tables. The purpose of this documentation is to describe the fields and information in each of the tables outlined below, as well as provide some summary statistics to better familiarize researchers with these data sets.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   umetrics_header
   usda_doc
   npo_doc



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
