
===========================================================================
UMETRICS - Organizational Units
===========================================================================
Ancilary Table Associated with UMETRICS Awards.

Field Information
---------------------------------------------------------------------------
+----------------+-------------------------------------------------------------------+
|          Field |                                                       Description |
+================+===================================================================+
|       CampusID |      Code for uniquely identifiying a specific university campus. |
+----------------+-------------------------------------------------------------------+
|     SubOrgUnit |             Two-digit ID assigned to the sub-organizational unit. |
+----------------+-------------------------------------------------------------------+
|     CampusName |                                               Name of the Campus. |
+----------------+-------------------------------------------------------------------+
| SubOrgUnitName |                         Full name of the sub-organizational unit. |
+----------------+-------------------------------------------------------------------+
| data_file_year | The year of the data file submission that this record resides in. |
+----------------+-------------------------------------------------------------------+

Source and Preprocessing
---------------------------------------------------------------------------

Data Source: Restricted VDI managed by the Institute for Research on Innovation & Science (IRIS)

Describe any preprocessing here.