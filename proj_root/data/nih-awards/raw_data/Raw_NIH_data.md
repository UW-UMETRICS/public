# National Institute of Health Award Data

This directory contains the raw data files for NIH awards which can be downloaded from <INSERT SOURCE URL>. Specifically it contains the following files:

 * RePORTER_PRJ_C_FY2008.csv
 * RePORTER_PRJ_C_FY2009.csv
 * RePORTER_PRJ_C_FY2010.csv
 * RePORTER_PRJ_C_FY2011.csv
 * RePORTER_PRJ_C_FY2012.csv
 * RePORTER_PRJ_C_FY2013.csv
 * RePORTER_PRJ_C_FY2014.csv
 * RePORTER_PRJ_C_FY2015.csv
 * RePORTER_PRJ_C_FY2016.csv