# UMETRICS University of Wisconsin Award Data

This directory contains processed data for UMETRICS awards at UW. Specifically it contains the following files:

 * UMETRICS_All_Years_Award.csv
 * UMETRICS_All_Years_Aggregated.csv
 * UMETRICS_All_Years_Employee.csv
 * UMETRICS_All_Years_EmployeeWNames.csv
 * UMETRICS_All_Years_ObjectCodes.csv
 * UMETRICS_All_Years_OrganizationalUnit.csv
 * UMETRICS_All_Years_Subaward.csv
 * UMETRICS_All_Years_Vendor.csv